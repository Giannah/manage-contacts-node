const User = require('../models/user')

class ContactController {
  /** Get all the contacts */

  async getContacts (req, res) {
    try {
      const results = await User.find()

      res
        .status(200)
        .json(results)
        .end()
    } catch (error) {
      res
        .status(400)
        .end()
    }
  }

  /** Get a contact by Id */

  async getContactById (req, res) {
    const { id } = req.params

    try {
      const contact = await User.findOne({
        _id: id
      })
      res
        .status(200)
        .json(contact)
        .end()
    } catch (error) {
      res
        .status(400)
        .end()
    }
  }

  /** Add a contact to the DB */

  async addContact (req, res) {
    const { firstName, lastName } = req.body

    try {
      const newUser = new User({ lastName, firstName })
      const result = await newUser.save()

      const socketio = req.app.get('socketIo')
      socketio.sockets.emit('newContact', { result: result })

      res
        .status(200)
        .json(result)
        .end()
    } catch (error) {
      res
        .status(400)
        .end()
    }
  }

  /** Update contact */

  async updateContact (req, res) {
    const { id } = req.params
    const { lastName, firstName } = req.body

    try {
      const result = await User.findByIdAndUpdate(
        { _id: id },
        { firstName, lastName },
        { new: true }
      )
      const socketio = req.app.get('socketIo')
      socketio.sockets.emit('updatedContact', { result: result })

      res
        .status(200)
        .json(result)
        .end()
    } catch (error) {
      res
        .status(403)
        .json(error)
        .end()
    }
  }

  /** Delete contact from DB */

  async deleteContact (req, res) {
    const { id } = req.params

    try {
      const result = await User.findOneAndDelete({ _id: id })
      const socketio = req.app.get('socketIo')
      socketio.sockets.emit('deletedContact', { result: result })
      res
        .status(200)
        .json(result)
        .end()
    } catch (error) {
      res
        .status(400)
        .json(error)
        .end()
    }
  }

  /** Front-End */

  async contactManager (req, res) {
    try {
      const results = await User.find()

      res
        .status(200)
        .render('contactsManager', { title: 'Contacts Manager', contacts: results })
        .end()
    } catch (error) {
      res
        .status(500)
        .end()
    }
  }
}

module.exports = ContactController
