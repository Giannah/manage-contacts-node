#!/usr/bin/env node;
/* eslint-disable max-classes-per-file */

const chalk = require('chalk')
const yargs = require('yargs')

class Contact {
  constructor (contact) {
    Object.assign(this, contact)
  }

  toString () {
    let lastName = this.lastName.toUpperCase()
    let { firstName } = this

    if (yargs.argv.c) {
      lastName = chalk.blue(lastName)
      firstName = chalk.red(firstName)
    }
    return `${lastName} ${firstName}`
  }
}
module.exports = { Contact }
