const path = 'contacts.json'
const _ = require('lodash')
const fs = require('fs')
const { Contact } = require('./Contact')

class FileContactService {
  read (callback) {
    fs.readFile(path, (err, result) => {
      if (err) {
        console.error('Error loading file: ', err)
      }
      try {
        const data = (JSON.parse(result))
        const contacts = data.map((contact) => new Contact(contact))
        if (callback) {
          callback(contacts)
        }
      } catch (error) {
        console.error('Error parsing file: ', error)
      }
    })
  }

  get (callback) {
    this.read(callback)
  }

  write (contacts, callback) {
    fs.writeFile(path, JSON.stringify(contacts), callback)
  }

  add (lastName, firstName, callback) {
    this.read((contacts) => {
      contacts.push(
        new Contact({
          lastName,
          firstName,
          id: _.last(contacts).id + 1
        })
      )

      this.write(contacts, callback)
    })
  }

  delete (id, callback) {
    this.read((contacts) => {
      this.write(contacts.filter((c) => c.id !== id), callback)
    })
  }

  print () {
    this.get((data) => console.log(data.join(',')))
  }

  watch () {
    this.read((knownContacts) => {
      fs.watch(path, () => {
        this.read((newContacts) => {
          console.log('watched', _.differenceWith(knownContacts, newContacts, _.isEqual))
        })
      })
    })
  }
}

module.exports = { FileContactService }
