const yargs = require('yargs')

function Cli (test) {
  yargs
    .version('0.0.0')
    .option('c', {
      alias: 'colors',
      desc: 'Styles your console with colors'
    })
    .help()
    .global('c')

  yargs
    .command({
      command: 'list',
      aliases: 'ls',
      desc: 'Lists all contacts',
      handler: () => test.print('3', 'hello', 10)
    })
    .command({
      command: 'add',
      desc: 'adds a contact to the list',
      builder: (ygs) => {
        ygs
          .option('firstname', {
            alias: 'f',
            desc: 'A contact\'s firstname',
            demand: true,
            type: 'string'
          })
          .option('lastname', {
            alias: 'l',
            desc: 'A contact\'s lastname',
            demand: true,
            type: 'string'
          })
      },
      handler: (argv) => test.add(argv.firstName, argv.lastName, () => test.print())
    })
    .command({
      command: 'delete',
      desc: 'deletes a contact from the list',
      builder: (ygs) => {
        ygs
          .option('id', {
            alias: 'i',
            desc: 'A contact\'s id',
            demand: true,
            type: 'number'
          })
      },
      handler: (argv) => test.delete(argv.id, () => test.print())
    })
    .command({
      command: 'watch',
      desc: 'watches for changes in the contacts list',
      handler: () => test.watch()
    })
    .command({
      command: 'serve',
      desc: 'Starts the server',
      builder: (ygs) => {
        ygs
          .option('port', {
            default: 3000,
            type: 'number'
          })
      },
      handler: (argv) => {
        const server = require('../Server')
        server(test, argv.port)
      }
    })
  // eslint-disable-next-line no-unused-expressions
  yargs.argv
}

module.exports = { Cli }
