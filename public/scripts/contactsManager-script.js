/* eslint-disable no-undef */
const deleteButton = document.querySelectorAll('.deleteButton')
const addContactForm = document.querySelector('.addContactForm')
const socket = io.connect()

socket.on('newContact', (e) => {
  const { firstName, lastName } = e.result
  const socketNews = document.querySelector('.socketNews')

  socketNews.classList.add('alert-primary')
  socketNews.innerHTML = `${firstName} ${lastName} à bien été ajouté à la liste`

  setTimeout(() => {
    socketNews.classList.remove('alert-primary')
    socketNews.innerHTML = ''
    location.reload()
  }, 2500)
})

socket.on('deletedContact', (e) => {
  const { firstName, lastName } = e.result
  const socketNews = document.querySelector('.socketNews')

  socketNews.classList.add('alert-danger')
  socketNews.innerHTML = `${firstName} ${lastName} à bien été retiré de la liste`
  setTimeout(() => {
    socketNews.classList.remove('alert-danger')
    socketNews.innerHTML = ''
    location.reload()
  }, 2500)
})

socket.on('updatedContact', (e) => {
  console.log(e)
})

deleteButton.forEach(b => {
  b.addEventListener('click', (e) => {
    const contactId = e.target.parentNode.id
    axios.delete(`http://localhost:8080/rest/contacts/delete/${contactId}`)
      .catch(err => console.error(err))
  })
})

addContactForm.addEventListener('submit', e => {
  e.preventDefault()
  const firstName = document.querySelector('#firstName').value
  const lastName = document.querySelector('#lastName').value
  const contact = {
    firstName,
    lastName
  }
  axios.post('http://localhost:8080/rest/contacts', contact)
    .catch(err => console.error(err))
})
