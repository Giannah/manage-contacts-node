const express = require('express')
const router = express.Router()
const ContactController = require('../controllers/contactsController')

const controller = new ContactController()

router.get('/', controller.getContacts)
router.post('/', controller.addContact)
router.get('/find/:id', controller.getContactById)
router.put('/update/:id', controller.updateContact)
router.delete('/delete/:id', controller.deleteContact)

module.exports = router
