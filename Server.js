require('dotenv').config()
const path = require('path')
const express = require('express')
const http = require('http')
const cors = require('cors')

const app = express()
const server = http.createServer(app)
const bodyParser = require('body-parser')
const io = require('socket.io').listen(server)

const mongoose = require('mongoose')

/** Contact controller */
const ContactController = require('./controllers/contactsController')
const controller = new ContactController()

/** Router */
const contactRouter = require('./router/contactsRouter')

/** Middlewares */
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE')
  next()
})
app.use(function (req, res, next) {
  next()
  console.info(`${req.method} ${req.path} ${res.statusCode}`)
})

/** API */
app.use('/rest/contacts', contactRouter)

/** DB connection */

const db = process.env.MONGO_URL_CONNECT

mongoose
  .connect(db, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected to DB !')
  })

  .catch((error) => {
    console.log('Oups something went wrong !', error)
  })

/** Web socket */
app.set('socketIo', io)
// io.on('connection', (socket) => {
//   console.log('Here we go !')

//   socket.on('disconnect', () => {
//     console.log('We\'re off !')
//   })
// })
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(express.static(path.join(__dirname, 'public')))

/** FRONT */
app.get('/manage-contacts', controller.contactManager)

server.listen(process.env.PORT || 8080, function () {
  console.log('Server is up and running on port: ' + server.address().port)
})
